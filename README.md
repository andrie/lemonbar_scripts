# lemonbar\_scripts

This is a collection of lemonbar scripts that I use.

# Dependencies

`~/.local/bin` must be in your `$PATH`.

`ttf-sourcecodepro-nerd` - Font for the status bar.

`bspwm` - Window manager.

`connman` - Network manager.

`mpd` - Music daemon.

`mpc` - Music player.

`brightnessctl` - Get brightness.

`pipewire` - Audio daemon. I have not tested pulseaudio but it should work too.

`xtitle` - Get window title.
# Customize it

I know that some of you may not use these programs.
You can change the font and the other programs by editing the scripts.

# How does it work

I am sorry if you found no comments in the code.
I will try to explain what it does in detail.

The `lemonbar_init` script is the main script, all the others are just modules.
You declare the modules you would like to use in the `modules` variable, which is in the `lemonbar_init` script.

The `panel_format()` function is the one who handles all the formatting of the bar.
It is on an infinite loop. 
It waits until a line has been sent to the FIFO pipe, then formats the lines accordingly.
The output is set to a variable so we can reuse it if it has not been updated yet.

`lemonbar_workspaces` sends out an output with the prefix `WORKSPACE` so the formatter knows where it came from.
`workspace=${line#WORKSPACES}` removes the prefix, then sets it to the variable `workspace`.

```
panel_format() {
    while read -r line; do
        case "$line" in
            WORKSPACE"*)
                workspace="${line#WORKSPACE}" 
                ;;
        # other lines ...
}
```

The variable `workspace` is then used in the `printf` command.

`lemonbar_init` then creates a FIFO pipe.
The purpose of a FIFO pipe is so you can refresh blocks dynamically.
This avoids reloading modules that don't need to be reloaded.

The trap command is there to kill all child processes on exit.
You would not want any orphans roaming around when `lemonbar_init` gets restarted.

The `panel_width` variable is the width of the monitor minus 20 pixels to make way for gaps.

Then, all the modules in the `modules` variable will get executed to the background.

Finally, the `panel_format()` function is called and its output will be piped into lemonbar, with some settings.

The settings can be found in `lemonbar_options`, where you can change various stuff.

# Disabling a module

If you wish to disable a module, remove the module in the `modules` variable.
Next, remove the line in `panel_format()` the 3 lines of code which receives the module output.
For example, you wish to remove the `lemonbar_workspaces`, you should remove this part:

```
"WORKSPACE"*)
    workspace="${line#WORKSPACE}" 
    ;;
```

Finally, remove the lines of code referencing the module in the `printf` command.

```        
printf "%s" \
"${workspace}" \ # remove this line
"%{r}" \
```

Now restart `lemonbar_init` and it should be gone. 

# One shot modules

There are 2 one shot modules, `lemonbar_brightness` and `lemonbar_volume`.
By one shot I mean they just execute one time and quit.
Basically, they never update again.
They don't have an infinite loop, so in order to trigger them, I bound them to my volume keys.
For example, in my sxhkdrc:

```
# Volume Up
XF86AudioRaiseVolume
	pamixer -i 5 ; lemonbar_volume
```

When I hit the volume key up, `lemonbar_volume` gets executed, so the volume percentage updates accordingly.

# Formatting

From `lemonbar(1)`: 

```
lemonbar provides a screenrc-inspired formatting syntax to allow full customization at runtime.
Every formatting block is opened with "%{" and closed by "}" and accepts the following commands, the parser tries its best to handle malformed input.
Use "%%" to get a literal percent sign ("%").
```

This is how you would set a custom font color. See `man lemonbar` for more details.

```
%{F#d79921} text with custom color %{F-} text with regular color
```

# Hide bar when full screen

Use `xdo lower -a _nameOfBar_`.
Check the name of your bar using `xprop`.

# Improvements

At first, I had `lemonbar_volume` be executed whenever `pactl subscribe` outputs a something.
It was a disaster.
Turns out, `pactl subscribe` outputs alot of stuff in such a short amount of time.
The module kept refreshing, so the CPU usage spiked up to 35%.
Now, its a one shot module.

Subscribing to events is nice, like `lemonbar_music`.
This is what I did with `lemonbar_net` at first, it wasn't as bad as `lemonbar_volume`.
But it still outputed alot of lines that I didn't really need.
I would like to quit the program when I receive a single line, then sleep for a few seconds, then execute the program again.
I don't know how to do this efficiently, all the stuff I came up with was bad.

# Author Comments

This took me a whole week to figure out stuff.
It was worth it, since I learned alot of shell scripting in the process.
Use polybar if you want a bar that is easy to configure.

